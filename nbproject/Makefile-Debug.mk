#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/MqttClientWorker.o \
	${OBJECTDIR}/src/ServerApp.o \
	${OBJECTDIR}/src/ServiceModel.o \
	${OBJECTDIR}/src/main.o \
	${OBJECTDIR}/thrift/gen-cpp/M2MWorkerService.o \
	${OBJECTDIR}/thrift/gen-cpp/m2mworker_constants.o \
	${OBJECTDIR}/thrift/gen-cpp/m2mworker_types.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=c++0x
CXXFLAGS=-std=c++0x

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../corelibs/upnet/lib/libupnet.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk bin/m2mworkerservice

bin/m2mworkerservice: ../corelibs/upnet/lib/libupnet.a

bin/m2mworkerservice: ${OBJECTFILES}
	${MKDIR} -p bin
	${LINK.cc} -o bin/m2mworkerservice ${OBJECTFILES} ${LDLIBSOPTIONS} ../corelibs/UPStoreAppCommon/lib/libupstoreappcommon.a ../corelibs/UPStorage/lib/libupstorage.a ../corelibs/UPDistributed/lib/libupdistributed.a ../corelibs/UPCaching/lib/libupcaching.a ../corelibs/UPHashing/lib/libuphashing.a ../corelibs/UPBase/lib/libupbase.a ../corelibs/UPThrift/lib/libupthrift.a ../corelibs/UPPoco/lib/libuppoco.a ../corelibs/UPEvent/lib/libupevent.a ../corelibs/UPMalloc/lib/libupmalloc.a -lpthread -ldl -lrt ../extralibs/libpahomqttv3.a

${OBJECTDIR}/src/MqttClientWorker.o: src/MqttClientWorker.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/upnet/include -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/MqttClientWorker.o src/MqttClientWorker.cpp

${OBJECTDIR}/src/ServerApp.o: src/ServerApp.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/upnet/include -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ServerApp.o src/ServerApp.cpp

${OBJECTDIR}/src/ServiceModel.o: src/ServiceModel.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/upnet/include -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ServiceModel.o src/ServiceModel.cpp

${OBJECTDIR}/src/main.o: src/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/upnet/include -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/main.o src/main.cpp

${OBJECTDIR}/thrift/gen-cpp/M2MWorkerService.o: thrift/gen-cpp/M2MWorkerService.cpp 
	${MKDIR} -p ${OBJECTDIR}/thrift/gen-cpp
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/upnet/include -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/thrift/gen-cpp/M2MWorkerService.o thrift/gen-cpp/M2MWorkerService.cpp

${OBJECTDIR}/thrift/gen-cpp/m2mworker_constants.o: thrift/gen-cpp/m2mworker_constants.cpp 
	${MKDIR} -p ${OBJECTDIR}/thrift/gen-cpp
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/upnet/include -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/thrift/gen-cpp/m2mworker_constants.o thrift/gen-cpp/m2mworker_constants.cpp

${OBJECTDIR}/thrift/gen-cpp/m2mworker_types.o: thrift/gen-cpp/m2mworker_types.cpp 
	${MKDIR} -p ${OBJECTDIR}/thrift/gen-cpp
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -I../corelibs/upnet/include -I/usr/local/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/thrift/gen-cpp/m2mworker_types.o thrift/gen-cpp/m2mworker_types.cpp

# Subprojects
.build-subprojects:
	cd ../../lib/pahomqttv3 && ${MAKE}  -f Makefile CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} bin/m2mworkerservice

# Subprojects
.clean-subprojects:
	cd ../../lib/pahomqttv3 && ${MAKE}  -f Makefile CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
