/*
 * File:   ServiceConfig.h
 * Author: anhn
 *
 * Created on November 13, 2014, 2:30 PM
 */

#ifndef SERVICECONFIG_H
#define	SERVICECONFIG_H
#include "Poco/Util/Application.h"
#include <string>
#include "PocoEx/NetUtil.h"
#include <iostream>
#include "ServiceModel.h"
#include "asio.hpp"

namespace Up {
    namespace Core {
	namespace M2M {
	    namespace Worker {

		class ServiceConfig {
		public:

		    std::string svrHost;
		    int svrPort;
		    int cfgPort;
		    int workerCount;
		    std::string name;

		    std::string brokerHost;
		    int brokerPort;

		    std::string zkServers;
		    std::string zkRegPath;
		    std::string zkScheme;

		    asio::io_service ioservice;

		    std::string mqttClientId;
		    std::string doorTopic;
                    
                    std::string mqttUserName;
                    std::string mqttPassword;

		    ServiceConfig() {
		    }

		    void init(Poco::Util::Application& app) {
			auto& cfg = app.config();

			if (svrPort <= 0) svrPort = app.config().getInt("sns.thrift.port", 8899);
			std::cout << "service port:" << svrPort << std::endl;

			if (cfgPort <= 0) cfgPort = app.config().getInt("sns.thrift.config.port", 8900);
			std::cout << "config port:" << cfgPort << std::endl;

			if (svrHost.empty()) svrHost = app.config().getString("sns.thrift.host", "10.30.");
			std::cout << "please check host :" << svrHost << std::endl;

			if (brokerPort <= 0) brokerPort = app.config().getInt("sns.broker.port", 1883);
			std::cout << "broker port:" << brokerPort << std::endl;

			if (brokerHost.empty()) brokerHost = app.config().getString("sns.broker.host", "127.0.0.1");
			std::cout << "please check brokeHost :" << brokerHost << std::endl;

			mqttClientId = app.config().getString("sns.mqttClient.id", "upmqtt.gw");

			if (workerCount <= 0) workerCount = app.config().getInt("sns.thrift.service.workercount", 5);
			std::cout << "worker count: " << workerCount << std::endl;

			if (name.empty()) svrHost = app.config().getString("sns.service.name", "m2mworkerservice");

			if (zkServers.empty()) zkServers = app.config().getString("sns.service.zkservers", "");
			if (zkRegPath.empty()) zkRegPath = app.config().getString("sns.service.reg_path", "/up-division/noconfig/services");
			if (zkScheme.empty()) zkScheme = app.config().getString("sns.service.scheme", "thrift_binary");

			doorTopic = app.config().getString("sns.doortopic", "VNG/doors");
                        
                        if (mqttUserName.empty()) {
                            mqttUserName = app.config().getString("sns.mqtt.username", "");
                        }
                        if (mqttPassword.empty()) {
                            mqttPassword = app.config().getString("sns.mqtt.password", "");
                        }
                        
                        std::cout << "Door Topic: " << doorTopic << std::endl;
		    }

		    static ServiceConfig& instance() {
			static ServiceConfig config;
			return config;
		    }
		};

	    }
	}
    }
}



#endif	/* SERVICECONFIG_H */

