/*
 * File:   Utils.h
 * Author: tiennd
 *
 * Created on December 29, 2014, 2:55 PM
 */

#ifndef UTILS_H
#define	UTILS_H

#include <string>

class Utils {
public:

    static const std::string toLower(const std::string& text) {
        std::string ret(text.size(), ' ');
        std::transform(text.begin(), text.end(), ret.begin(), ::tolower);
        return ret;
    }

    static void traceFunction(const std::string& strFunc) {
#ifdef DLOG
        std::cout << strFunc << " is called\n";
#endif
    }

    static void tracePagram(int num, ...) {
#ifdef DLOG
        va_list vl;
        va_start(vl, num);
        for (int i = 0; i < num; ++i) {
            std::cout << va_arg(vl, const char *);
        }
        std::cout << "\n";
#endif
    }
};

#define TRACE_FUNCTION Utils::traceFunction(__FUNCTION__)

#endif	/* UTILS_H */

