/*
 * File:   ServiceHandler.h
 * Author: tiennd
 *
 * Created on December 16, 2014, 9:58 AM
 */

#pragma once

#ifndef SERVICEHANDLER_H
#define	SERVICEHANDLER_H


#include "M2MWorkerService.h"
#include "ServiceModel.h"
#include "ServiceThriftHandlerBaseT.h"

namespace Up {
    namespace Core {
	namespace M2M {
	    namespace Worker {

		class ServiceHandler : public TServiceThriftHandlerBaseT<ServiceModel,
		M2MWorkerServiceIf, M2MWorkerServiceProcessor,
		::apache::thrift::protocol::TCompactProtocolFactory > {
		public:
		    typedef TServiceThriftHandlerBaseT<ServiceModel,
		    M2MWorkerServiceIf, M2MWorkerServiceProcessor,
		    ::apache::thrift::protocol::TCompactProtocolFactory > _Base;

		    ServiceHandler(Poco::SharedPtr<ServiceModel> aModel) : _Base(aModel) {
		    };

		public:

		    virtual void openDoor(ErrorCode& _return, const int16_t doorId, const int32_t userId) {
			if (m_pmodel) m_pmodel->openDoor(_return, doorId, userId);
		    }



		};


	    }
	}
    }
}









#endif	/* SERVICEHANDLER_H */

