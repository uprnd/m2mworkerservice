/* 
 * File:   MqttClientWorker.h
 * Author: huuhoa
 *
 * Created on June 23, 2015, 11:15 AM
 */

#ifndef MQTTCLIENTWORKER_H
#define	MQTTCLIENTWORKER_H

#include <vector>
#include <string>
#include <stdint.h>

extern "C" {
    #include "MQTTClient.h"
    #include "MQTTClientPersistence.h"
}

namespace Up {
namespace Core {
namespace M2M {
namespace Worker {

class ServiceModel;

class MqttClientWorker {
public:
    static std::shared_ptr<MqttClientWorker> createClient(ServiceModel *serviceModel, const std::string& clientId, const std::string& addr, int port) {
        return std::shared_ptr<MqttClientWorker>(new MqttClientWorker(serviceModel, clientId, addr, port));
    }
    void setAuthenticationParameters(const std::string& username, const std::string& password);
    virtual ~MqttClientWorker() {}

public:
    virtual void onConnack(uint8_t retCode);
    virtual void onMsg(std::string& topic, std::string& msg);
    
    void preSubscribe(std::string& topic, int qos);
    void autoReconnect(bool value) {}
    void beginConnect() {}
    void connect();
    
    bool isConnected();
    bool publish(const std::string& topic, const std::string& message);
protected:
    MqttClientWorker(ServiceModel *serviceModel, const std::string& clientId, const std::string& addr, int port);
protected:
    static void mqttDelivered(void *context, int dt);
    static int mqttMessageArrived(void *context, char *topicName, int topicLen, MQTTClient_message *message);
    static void mqttConnectionLost(void *context, char *cause);
private:
    std::string clientId_;
    std::string address_;
    int port_;
    std::vector<std::string> topicList_;
    std::vector<uint8_t> qosList_;
    ServiceModel *serviceModel_;
    MQTTClient client_;
    bool isConnected_;
    std::string username_;
    std::string password_;
};

}
}
}
}
#endif	/* MQTTCLIENTWORKER_H */

