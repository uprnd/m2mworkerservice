/*
 * File:   ExeptionSever.h
 * Author: tiennd
 *
 * Created on December 16, 2014, 4:09 PM
 */

#ifndef EXEPTIONSEVER_H
#define	EXEPTIONSEVER_H

#include <string>
#include "m2mworker_types.h"

//enum ErrorServer {
//    ERR_NONE = 0,
//    ERR_CONNECTION,
//    ERR_UNKNOWN,
//    ERR_TIMEOUT
//
//};

class ExceptionServer : public std::exception {
public:

    /* error connection */
    ExceptionServer(const std::string& nameServer, int line, const std::string& file)
    : _errorCode(Up::Core::M2M::Worker::ErrorServer::ERR_CONNECTION), _line(line), _file(file) {
        _errorMessage = ("error connection by ") + nameServer;
    }

    /* forward error */
    ExceptionServer(int type, const std::string& message, int line, const std::string& file)
    : _errorCode(type), _errorMessage(message), _line(line), _file(file) {
    }

    /* error by server no errorMessage */
    ExceptionServer(const std::string& nameServer, int type, int line, const std::string& file)
    : _errorCode(type), _line(line), _file(file) {
        switch (type) {
            case Up::Core::M2M::Worker::ErrorServer::ERR_UNKNOWN:
                _errorMessage = "error unknown by " + nameServer;
                break;
            case Up::Core::M2M::Worker::ErrorServer::ERR_TIMEOUT: 
                _errorMessage = "timeout while waiting for controller's feedback";
                break;
            default:
                _errorMessage = "error unknown by" + nameServer;
                break;
        }
    }

    virtual ~ExceptionServer() throw () {
    }

    int getError() const {
        return _errorCode;
    }

    std::string getErrorMessage() const {
        return _errorMessage;
    }

    int getLine() const {
        return _line;
    }

    std::string getFile() const {
        return _file;
    }


private:
    int _errorCode;
    std::string _errorMessage;
    int _line;
    std::string _file;
};


/* forward error */
#define EXCEPTION_SERVER(type, message) ExceptionServer(type, message,  __LINE__, __FILE__ )

/* error by server no errorMessage */
#define EXCEPTION_SERVER_TYPE(nameServer, type) ExceptionServer(std::string(nameServer), type, __LINE__, __FILE__ )

/* error connection */
#define EXCEPTION_SERVER_CONNECTION(nameServer) ExceptionServer( std::string(nameServer),  __LINE__, __FILE__ )


#define MQTT "mqttservice"


#endif	/* EXEPTIONSEVER_H */

