/*
 * File:   ServiceModel.h
 * Author: anhn
 *
 * Created on November 13, 2014, 11:49 AM
 */

#ifndef SERVICEMODEL_H
#define	SERVICEMODEL_H

#include <string>
#include <map>
#include <memory>
#include "m2mworker_types.h"
#include "Poco/SharedPtr.h"


//namespace upmqtt {
//class MqttClient;
//}


namespace Up {
namespace Core {
namespace M2M {
namespace Worker {
    
class MqttClientWorker;

class ServiceModel {
public:

    static Poco::SharedPtr<ServiceModel> instance() {
        static Poco::SharedPtr<ServiceModel> aModel;
        if (!aModel) aModel = new ServiceModel();
        return aModel;
    }

    void openDoor(ErrorCode& _return, const int16_t doorId, const int32_t userId);

    void onReceiveMqttMessage(std::string& topic, std::string& msg);
private:
    ServiceModel();
    int32_t waitForMessage(const int64_t ackToken, const int16_t timeout);
private:
    typedef std::map<int64_t, int16_t> MapTokenToInt16;
    
    std::shared_ptr<MqttClientWorker> _mqttClientPtr;
    MapTokenToInt16 waitingToken_;
    MapTokenToInt16 feedbackHolder_;
};

}
}
}
}


#endif	/* SERVICEMODEL_H */

