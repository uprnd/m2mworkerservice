namespace java vng.up.core.m2m.worker
namespace cpp Up.Core.M2M.Worker

enum ErrorServer
{
    ERR_NONE = 0,
    ERR_FEEDBACK_NOK,   
    ERR_CONNECTION,
    ERR_TIMEOUT,
    ERR_UNKNOWN
}

struct ErrorCode
{
    1: i32 code,
    2: optional string message
}

service M2MWorkerService
{
    ErrorCode openDoor(1:i16 doorId, 2:i32 userId)
}

