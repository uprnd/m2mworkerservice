
extern "C" {
//    #include "MQTTAsync.h"
    #include "MQTTClient.h"
    #include "MQTTClientPersistence.h"
}
#include <vector>
#include <iostream>
#include <memory>
#include <string>
#include <string.h>
#include <sys/param.h>


#include "ServiceModel.h"
#include "MqttClientWorker.h"


namespace Up {
namespace Core {
namespace M2M {
namespace Worker {
    
MqttClientWorker::MqttClientWorker(
        ServiceModel *serviceModel, 
        const std::string& clientId,
        const std::string& addr,
        int port)
    : clientId_(clientId),
        address_(addr),
        port_(port),
        serviceModel_(serviceModel),
        isConnected_(false),
        username_(),
        password_()
{
    std::cout << "Creating mqtt client" << std::endl;
    char buffer[MAXPATHLEN];
    memset(buffer, 0, MAXPATHLEN);
    sprintf(buffer, "tcp://%s:%d", addr.c_str(), port);
    
    MQTTClient_create(&client_, buffer, clientId_.c_str(),
        MQTTCLIENT_PERSISTENCE_NONE, NULL);
    MQTTClient_setCallbacks(
            client_,
            this,
            &MqttClientWorker::mqttConnectionLost,
            &MqttClientWorker::mqttMessageArrived,
            &MqttClientWorker::mqttDelivered);
}

void MqttClientWorker::setAuthenticationParameters(const std::string& username, const std::string& password) {
    username_ = username;
    password_ = password;
}

void MqttClientWorker::mqttDelivered(void *context, int dt) {
    MqttClientWorker *this_ = static_cast<MqttClientWorker*>(context);
    if (!this_) {
        return;
    }
    
    std::cout << "Message delivered: " << dt << std::endl;
}

namespace Utils {
std::string strncpy(const char* str, const size_t n)
{
    if (str == NULL || n == 0)
    {
        return std::string();
    }

    return std::string(str, std::min(strlen(str), n));
}
}

int MqttClientWorker::mqttMessageArrived(void *context, char *topicName, int topicLen, MQTTClient_message *message) {
    MqttClientWorker *this_ = static_cast<MqttClientWorker*>(context);
    if (!this_) {
        return 0;
    }
    
    int i;
    char* payloadptr;

    printf("Message arrived\n");
    printf("     topic: %s\n", topicName);
    printf("   message: ");

    payloadptr = (char*)message->payload;
    for(i=0; i<message->payloadlen; i++)
    {
        putchar(*payloadptr++);
    }
    putchar('\n');
    std::string topic(topicName);
    std::string msg((char*)message->payload, message->payloadlen);
    this_->serviceModel_->onReceiveMqttMessage(topic, msg);
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;    
}

void MqttClientWorker::mqttConnectionLost(void *context, char *cause) {
    std::cout << "\nConnection lost\n";
    std::cout << "     cause: " << cause << std::endl;
}

bool MqttClientWorker::isConnected() {
    return MQTTClient_isConnected(client_);
}

void MqttClientWorker::onConnack(uint8_t retCode) {
    std::cout << "onConnack" << std::endl;
    if (!topicList_.empty() && !qosList_.empty()) {
        std::cout << "Subscribe to topic: " << topicList_.front() << std::endl;
        for (uint32_t i=0; i<topicList_.size(); i++) {
            std::string topic = topicList_[i];
            int qos = qosList_[i];
            MQTTClient_subscribe(client_, topic.c_str(), qos);
        }
    }
}

void MqttClientWorker::onMsg(std::string& topic, std::string& msg) {
    serviceModel_->onReceiveMqttMessage(topic, msg);
}
    
void MqttClientWorker::preSubscribe(std::string& topic, int qos) {
    if (isConnected()) {
        MQTTClient_subscribe(client_, topic.c_str(), qos);
    } else {
        topicList_.push_back(topic);
        qosList_.push_back(qos);
    }
}

void MqttClientWorker::connect() {
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    int rc;

    if (!username_.empty()) {
        conn_opts.username = username_.c_str();
    }
    
    if (!password_.empty()) {
        conn_opts.password = password_.c_str();
    }
    conn_opts.keepAliveInterval = 60;
    conn_opts.cleansession = 1;

    if ((rc = MQTTClient_connect(client_, &conn_opts)) != MQTTCLIENT_SUCCESS)
    {
        printf("Failed to connect, return code %d\n", rc);
        exit(-1);
    }
    
    isConnected_ = true;
    onConnack(0);
}

bool MqttClientWorker::publish(const std::string& topic, const std::string& message) {
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken token;
    int rc;
    
    pubmsg.payload = (void *)message.c_str();
    pubmsg.payloadlen = message.length();
    pubmsg.qos = 0;
    pubmsg.retained = 0;
    MQTTClient_publishMessage(client_, topic.c_str(), &pubmsg, &token);
//    printf("Waiting for up to %d seconds for publication of %s\n"
//            "on topic %s for client with ClientID: %s\n",
//            (int)(TIMEOUT/1000), PAYLOAD, TOPIC, CLIENTID);
//    rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
//    printf("Message with delivery token %d delivered\n", token);
//    MQTTClient_disconnect(client, 10000);
//    MQTTClient_destroy(&client);    
    return token;
}
}
}
}
}


