
#include <iostream>
#include <memory>
#include <string>
#include <Poco/RegularExpression.h>

#include "ServiceConfig.h"
#include "ServiceHandler.h"
#include "ServiceModel.h"
#include "ExceptionServer.h"
#include "Utils.h"
#include "MqttClientWorker.h"


using namespace std;

namespace Up {
namespace Core {
namespace M2M {
namespace Worker {
    
ServiceModel::ServiceModel() {
    _mqttClientPtr = MqttClientWorker::createClient(this,
//            ServiceConfig::instance().ioservice,
	    ServiceConfig::instance().mqttClientId,
	    ServiceConfig::instance().brokerHost,
	    ServiceConfig::instance().brokerPort
	    );
    cout << "Service is started" << endl;
    _mqttClientPtr->setAuthenticationParameters(
            ServiceConfig::instance().mqttUserName,
            ServiceConfig::instance().mqttPassword);
    _mqttClientPtr->preSubscribe(ServiceConfig::instance().doorTopic, 0);
    _mqttClientPtr->autoReconnect(true);
    _mqttClientPtr->beginConnect();
    _mqttClientPtr->connect();
}

void ServiceModel::openDoor(ErrorCode& _return, const int16_t doorId, const int32_t userId) {

    // FIXME
    TRACE_FUNCTION;

    std::stringstream ss;
    if (_mqttClientPtr->isConnected()) {
        int64_t ackToken = ((int64_t)userId) << 32 | (doorId);
        if (waitingToken_.find(ackToken) == waitingToken_.end()) {
            // there is no waiting
            // add one
            waitingToken_[ackToken] = 1;
            feedbackHolder_[ackToken] = -1;
        } else {
            // increase counter
            waitingToken_[ackToken] = waitingToken_[ackToken] + 1;
            feedbackHolder_[ackToken] = -1;
        }
        
	ss << "OPEN " << doorId << " " << ackToken << " " << waitingToken_[ackToken];
	_mqttClientPtr->publish(ServiceConfig::instance().doorTopic, ss.str());
	_return.code = waitForMessage(ackToken, 10000);

    } else {
	_mqttClientPtr->connect();

	ExceptionServer e = EXCEPTION_SERVER_CONNECTION(MQTT);
	_return.code = e.getError();
	_return.message = e.getErrorMessage();
	_return.__isset.message = true;
	return;
    }
}

void ServiceModel::onReceiveMqttMessage(std::string& topic, std::string& msg) {
    TRACE_FUNCTION;
    
    cout << "Topic: " << topic << ": " << msg << endl;
    static const Poco::RegularExpression expression("ACK OPEN (\\d+) (\\d+) (OK|NOK)");
    Poco::RegularExpression::MatchVec what;
    if (expression.match(msg, 0, what) > 0) {
        // what[0] contains the whole string 
        // what[1] contains the door id
        // what[2] contains the ack token
        // what[3] contains the feedback result
        std::string doorId = msg.substr(what[1].offset, what[1].length);
        std::string ackTokenId = msg.substr(what[2].offset, what[2].length);
        std::string feedbackResult = msg.substr(what[3].offset, what[3].length);
        
        cerr << "Receive ACK for door " << std::atoi(doorId.c_str());
        int64_t ackToken;
        std::istringstream ss(ackTokenId);
        ss >> ackToken;
        if (feedbackResult.compare("OK") == 0) {
            feedbackHolder_[ackToken] = 1;
        } else if (feedbackResult.compare("NOK") == 0) {
            feedbackHolder_[ackToken] = 0;
        } else {
            feedbackHolder_[ackToken] = -1;
        }
        waitingToken_[ackToken] = 0;
    }
}

namespace Timing {

#if defined(WIN32) || defined(WIN64)
#define START_TIME_TYPE DWORD
START_TIME_TYPE start_clock(void)
{
	return GetTickCount();
}
#elif defined(AIX)
#define START_TIME_TYPE struct timespec
START_TIME_TYPE start_clock(void)
{
	static struct timespec start;
	clock_gettime(CLOCK_REALTIME, &start);
	return start;
}
#else
#define START_TIME_TYPE struct timeval
START_TIME_TYPE start_clock(void)
{
	static struct timeval start;
	gettimeofday(&start, NULL);
	return start;
}
#endif


#if defined(WIN32) || defined(WIN64)
long elapsed(DWORD milliseconds)
{
	return GetTickCount() - milliseconds;
}
#elif defined(AIX)
#define assert(a)
long elapsed(struct timespec start)
{
	struct timespec now, res;

	clock_gettime(CLOCK_REALTIME, &now);
	ntimersub(now, start, res);
	return (res.tv_sec)*1000L + (res.tv_nsec)/1000000L;
}
#else
long elapsed(struct timeval start)
{
	struct timeval now, res;

	gettimeofday(&now, NULL);
	timersub(&now, &start, &res);
	return (res.tv_sec)*1000 + (res.tv_usec)/1000;
}
#endif    
}

int32_t ServiceModel::waitForMessage(const int64_t ackToken, const int16_t timeout) {
    TRACE_FUNCTION;
    
    START_TIME_TYPE start = Timing::start_clock();
    unsigned long elapsed = 0L;
    
    elapsed = Timing::elapsed(start);
    while (elapsed < timeout)
    {
//        Thread_unlock_mutex(mqttclient_mutex);
//        MQTTClient_yield();
//        Thread_lock_mutex(mqttclient_mutex);
        
        if (waitingToken_[ackToken] == 0) {
            if (feedbackHolder_[ackToken] == 0) {
                return ErrorServer::ERR_FEEDBACK_NOK;
            } else if (feedbackHolder_[ackToken] == 1) {
                return ErrorServer::ERR_NONE;
            } else {
                return ErrorServer::ERR_UNKNOWN;
            }
        }
        elapsed = Timing::elapsed(start);
    }
    
    std::cout << "Timeout" << std::endl;
    return ErrorServer::ERR_TIMEOUT;
}

}
}
}
}

