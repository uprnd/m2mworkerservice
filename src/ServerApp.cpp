#include <monitor/TStorageStatModule.h>
#include <monitor/TStorageMonitorThriftHandler.h>
#include <ServerApp.h>
#include <Poco/Util/HelpFormatter.h>
#include <iostream>
#include "ServiceConfig.h"
#include "ServiceModel.h"
#include "ServiceHandler.h"
#include "Utils.h"

using namespace Poco::Util;
using namespace std;
using namespace Up::Core::M2M::Worker;

ServerApp::ServerApp() : _showHelp(false) {
}

ServerApp::~ServerApp() {
}

void ServerApp::initialize(Poco::Util::Application& app) {

    // FIXME
    TRACE_FUNCTION;

    if (_showHelp)
	return;

    loadConfiguration();

    ServiceConfig::instance().init(app);

    ServiceHandler::TServiceThriftServer *aBizServer = new ServiceHandler::TServiceThriftServer(
	    ServiceConfig::instance().svrPort
	    , ServiceConfig::instance().workerCount
	    , boost::shared_ptr< ServiceHandler > (new ServiceHandler(ServiceModel::instance())));

    this->addSubsystem(aBizServer);
    this->addSubsystem(new TStorageStatModule);

    // Stat handler
    TStorageMonitorThriftHandler* aStatHandler = new TStorageMonitorThriftHandler;
    this->addSubsystem(new TStorageMonitorThriftHandler::TStorageMonitorServer(
	    ServiceConfig::instance().cfgPort
	    , 2
	    , boost::shared_ptr< up::up101::storage::monitor::StorageMonitorServiceIf > (aStatHandler)
	    ));

    Poco::Util::ServerApplication::initialize(app);

}

int ServerApp::main(const std::vector<std::string>& args) {

    if (config().getBool("app.showhelp", false)) {
	showHelp();
	return 0;
    }

    try {
	this->_zkReg.setZkHosts(ServiceConfig::instance().zkServers);
	if (this->_zkReg.registerActiveService(ServiceConfig::instance().zkRegPath, ServiceConfig::instance().svrHost, ServiceConfig::instance().svrPort, ServiceConfig::instance().zkScheme)) {
	    logger().information("Registered with zookeeper");
	}
	this->_zkMonitorReg.setZkHosts(ServiceConfig::instance().zkServers);
	if (this->_zkMonitorReg.registerActiveService("/up-monitor/" + ServiceConfig::instance().zkRegPath, ServiceConfig::instance().svrHost, ServiceConfig::instance().cfgPort, "thrift_binary")) {
	    logger().information("Registered monitor with zookeeper");
	}

	auto &ios = ServiceConfig::instance().ioservice;
	ios.run();

    } catch (...) {

    }

    if (!_showHelp) waitForTerminationRequest();

    return 0;
}

void ServerApp::defineOptions(Poco::Util::OptionSet& options) {
    Poco::Util::ServerApplication::defineOptions(options);

    options.addOption(Poco::Util::Option("help", "help")
	    .description("show help messages")
	    .argument("showhelp", false)
	    .binding("app.showhelp")
	    );
}
